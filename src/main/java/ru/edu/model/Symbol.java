package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Интерфейс информации о курсе обмена
 */
public interface Symbol {

    String getSymbol();

    BigDecimal getPrice();

    /**
     * Время получения данных.
     *
     * @return
     */
    Instant getTimeStamp();
}
